Hackmeeting 0x7e3
=================

Sitio web para el Hackmeeting 2019 escrito en Markdown y procesado con [Hugo](https://gohugo.io/).

Quiero editar!
--------------

Las páginas estáticas están en /content.

La página de inicio está en themes/hackmeeting-hugo/layouts/index.html.


