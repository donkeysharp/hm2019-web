+++
title = "Ubicación"
+++

## Sede: Centro Cultural "Dragón Wari"

<iframe style="width: 100%; height: 500px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-68.14277529716493%2C-16.510308576790766%2C-68.13978195190431%2C-16.508747598462378&amp;layer=mapnik&amp;marker=-16.509526803388088%2C-68.1412786245346" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-16.50953&amp;mlon=-68.14128#map=19/-16.50953/-68.14128">Ver mapa más grande</a></small>


El Centro Cultural "Dragón Wari" está ubicado en la esquina de las calles José Saravia y Pioneros de Rochdale, a dos cuadras del Estadio Bolívar (Tembladerani) y la Av. Landaeta.

Para personas que viajen a La Paz para asistir al evento, pronto anunciaremos opciones para la estadía en la ciudad.

<img style="display: block; margin: 0 auto;" src="/img/llama.jpg" alt="Imagen: el Dragón Wari se destaca por una cabeza de llama" />
