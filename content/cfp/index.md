+++
# date = "2019-06-07"
title = "Call For Papers"
+++
<p style="text-align: center;">
¿Quiénes somos?<br />
¿A dónde queremos ir?<br /><br />
¿Podemos aún crear belleza con las máquinas?
</p>

## ¡Buscamos expositores!

En un congreso o conferencia común, unos pocos realizan el trabajo duro. Pero el Hackmeeting es más dinámico: todos somos expertos en algo, e intercambiamos libremente nuestro conocimiento.
El hecho de que todos aporten su granito de arena es lo que ha permitido al Hackmeeting alcanzar su décimo aniversario este año como un evento abierto y sin fines de lucro.

Además de nuestras Charlas Improvisadas (puedes ofrecer dar una en el momento del evento), tenemos espacios dedicados para Charlas Programadas, Talleres y Charlas Secretas. Si deseas apartar un ambiente y horario dedicado para tu charla o taller, puedes postular a nuestro Call For Papers **hasta el día lunes 15 de Julio** en el siguiente enlace:

[Postulación CFP Hackmeeting 0x7e3 La Paz](https://formularios.l10e.net/index.php?r=survey/index&sid=128877&lang=es)

## Temas

Aunque no nos restringimos a ciertas áreas exclusivas, el Hackmeeting presentó lo siguiente en anteriores ediciones:

  * Hacking y Seguridad Informática
    * Criptografía
    * Pentesting
    * Desarrollo seguro
    * Demostraciones de vulnerabilidades
    * Seguridad en la Nube
    * Ingeniería Social
  * Software Libre
    * Distribuciones GNU/Linux
  * Cultura Hacker
  * Nuevas tecnologías y herramientas
    * Inteligencia Artificial
  * Videojuegos
  * Proyectos ciudadano-comunitarios
    * Redes Libres - Wireless Mesh Networks
    * OpenStreetMap
  * Privacidad
  * Más Hacking
    * Lockpicking
    * Raspberry Pi

## Contacto

La comisión del CFP tiene una dirección de correo dedicado para consultas, casos especiales, etc.:

cfp [@] hackmeeting.org.bo

GPG está disponible (por ejemplo, puede necesitarse para coordinar charlas secretas). La clave es [D107 D039 1F07 2910 F634 B861 35BC 0BF4 ECE9 0A95](https://pgp.mit.edu/pks/lookup?search=0x35BC0BF4ECE90A95&op=vindex).
