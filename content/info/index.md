+++
# date = "2019-06-07"
title = "Información"
+++

## Preguntas Frecuentes

### ¿qué es Hackmeeting?

Citemos el texto de [hackmeeting.org](https://es.hackmeeting.org/hm/index.php?title=Manual:Portada):

> La palabra Hackmeeting fue acuñada a finales de los 90, en Italia, para referirse a un espacio donde se reúnen hackers y activistas sociales.

> Hackmeeting es un experimento desprovisto de acartonados formatos y rígidas pre-determinaciones. Es un espacio donde los nicks se convierten en personas y donde las asambleas virtuales devienen acción efectiva desde el punto de vista intelectual, afectivo y social para avanzar desarrollos libertarios.

> Desde el año 2000, esta comunidad heterogénea de apasionados por las tecnologías de la información, ha ido reuniéndose anualmente y en distintas localidades. Nos convoca el deseo de compartir con otras personas lo que hemos aprendido, de proponer la realización de acciones y proyectos conjuntos, de denunciar situaciones que nos afectan a todos y conocernos cara a cara para dar a nuestras relaciones un calor y un color que complementa nuestros sus afectos on-line.

> El hackmeeting pasa por ti, y tú pasas por él... Al volver a casa te quedas con el buen sabor de lo efímero, con la pasión por lo complejo, con la curiosidad por lo desconocido, con la crítica frente a lo injusto, con un proyecto, con una idea, con el deseo de volver.

En la actualidad, el Hackmeeting es un evento libre y abierto donde varios miembros de comunidades [hacker](https://www.elmundo.es/tecnologia/2014/10/27/544dea28ca474156028b456b.html) se reúnen una vez al año para realizar actividades relacionadas con las nuevas tecnologías y/o las problemáticas actuales (Seguridad informática, Software Libre, Hacklabs, Redes Libres, Soberanía digital, etc.).

Se suelen realizar ciclos de conferencias y _desconferencias_, las comunidades pueden exponer sus trabajos, organizar talleres o mesas redondas de discusión; además de festejar en conjunto los logros obtenidos y empezar a coordinar esfuerzos para los desafíos del futuro.

Aunque el Hackmeeting se celebra también en otras partes de Europa y Latinoamérica, la organización es independiente por cada país.

<!-- 
### ¿Cómo inició el evento?

El evento ha iniciado como una reunión de amigos donde se decidió abrir las puertas al público. En la página de [Historia del Hackmeeting](/historia/) tenemos más.

-->

### ¿Cómo va la organización para este año?

El Hackmeeting **ha alcanzado su décimo aniversario este año**, y siendo un evento autogestionado y sin fines de lucro, creemos que es un logro importante y estamos dispuestos a dar lo mejor de nosotros para este evento.

El evento será en la ciudad de **La Paz**, más concretamente en el [Centro Cultural "Dragón Wari"](/ubicacion/) por tres días consecutivos: viernes 20, sabado 21 y domingo 22 de septiembre. Personas del interior y exterior del país son bienvenidas: estaremos anunciando opciones de estadía en la ciudad pronto.

